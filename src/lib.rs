#![feature(core_intrinsics)]
use std::ops::*;
use std::fmt;
use std::fmt::Write;
use std::cmp::Ordering;

#[allow(non_camel_case_types)]
#[derive(Copy, Clone, PartialEq, PartialOrd)]
#[repr(C)]
/// Fast, finite, floating-point
pub struct fff(pub f32);

/// It's necessary for literals: 0.fff()
pub trait ToFFF32 {
    fn fff(&self) -> fff;
}

macro_rules! impl_cast {
    ($ty:ty) => {
        impl ToFFF32 for $ty {
            #[inline(always)]
            fn fff(&self) -> fff {
                fff(*self as f32)
            }
        }
    }
}

impl_cast! {i8}
impl_cast! {u8}
impl_cast! {i16}
impl_cast! {u16}
impl_cast! {i32}
impl_cast! {u32}
impl_cast! {f32}
impl_cast! {f64}
impl_cast! {usize}
impl_cast! {isize}

/// Not all float functions are wrapped/implemented in a wrapped/fast version,
/// but all should work by falling back to a regular f32 via Deref.
impl fff {
    #[inline(always)]
    pub fn min<V: Into<f32>>(self, min: V) -> Self {
        let min = min.into();
        if !(self.0 > min) { self } else { fff(min) }
    }

    #[inline(always)]
    pub fn max<V: Into<f32>>(self, max: V) -> Self {
        let max = max.into();
        if !(self.0 < max) { self } else { fff(max) }
    }

    #[inline(always)]
    /// Always returns true
    pub fn is_finite(&self) -> bool {
        debug_assert!(self.0.is_finite());
        true
    }

    #[inline(always)]
    pub fn as_usize(&self) -> usize {
        self.0 as usize
    }

    #[inline(always)]
    pub fn as_f32(&self) -> f32 {
        self.0
    }

    #[inline(always)]
    pub fn as_u16(&self) -> u16 {
        self.0 as u16
    }

    #[inline(always)]
    pub fn as_u8(&self) -> u8 {
        self.0 as u8
    }

    #[inline(always)]
    pub fn powf<V: Into<f32>>(self, v: V) -> Self {
        fff(self.0.powf(v.into()))
    }

    #[inline(always)]
    pub fn powi(self, v: i32) -> Self {
        fff(self.0.powi(v))
    }

    #[inline(always)]
    pub fn sqrt(self) -> Self {
        fff(unsafe{std::intrinsics::sqrtf32(self.0)})
    }

    #[inline(always)]
    /// Very slow. Use trunc()
    pub fn round(self) -> Self {
        self.0.round().into()
    }

    #[inline(always)]
    /// Very slow. Use trunc()
    pub fn floor(self) -> Self {
        self.0.floor().into()
    }

    #[inline(always)]
    /// Very slow. Use trunc()
    pub fn ceil(self) -> Self {
        self.0.ceil().into()
    }

    #[inline(always)]
    /// Inaccurate for values that don't fit in i32
    pub fn trunc(self) -> Self {
        fff(self.0 as i32 as f32)
    }

    #[inline(always)]
    pub fn abs(self) -> Self {
        self.0.abs().into()
    }
}

macro_rules! impl_fast {
    ($tr:ident, $fn:ident, $func:ident) => {
        impl $tr for fff {
            type Output = fff;

            #[inline(always)]
            fn $fn(self, other: fff) -> Self::Output {
                unsafe {
                    fff(std::intrinsics::$func(self.0, other.0))
                }
            }
        }

        impl $tr<f32> for fff {
            type Output = fff;

            #[inline(always)]
            fn $fn(self, other: f32) -> Self::Output {
                unsafe {
                    std::intrinsics::$func(self.0, other).into()
                }
            }
        }

        impl $tr<fff> for f32 {
            type Output = fff;

            #[inline(always)]
            fn $fn(self, other: fff) -> Self::Output {
                unsafe {
                    std::intrinsics::$func(self, other.0).into()
                }
            }
        }
    }
}

macro_rules! impl_assign {
    ($tr:ident, $func:ident, $fn:ident) => {
        impl $tr for fff {
            #[inline(always)]
            fn $fn(&mut self, other: fff) {
                *self = self.$func(other)
            }
        }
    }
}

impl_fast! {Add, add, fadd_fast}
impl_assign! {AddAssign, add, add_assign}
impl_fast! {Sub, sub, fsub_fast}
impl_assign! {SubAssign, sub, sub_assign}
impl_fast! {Mul, mul, fmul_fast}
impl_fast! {Rem, rem, frem_fast}
impl_fast! {Div, div, fdiv_fast}

impl Neg for fff {
    type Output = fff;
    fn neg(self) -> Self::Output {
        fff(self.0.neg())
    }
}

impl Eq for fff {
}

impl PartialEq<f32> for fff {
    #[inline(always)]
    fn eq(&self, other: &f32) -> bool {
        self.0.eq(other)
    }
    #[inline(always)]
    fn ne(&self, other: &f32) -> bool {
        self.0.ne(other)
    }
}

impl PartialEq<fff> for f32 {
    #[inline(always)]
    fn eq(&self, other: &fff) -> bool {
        self.eq(&other.0)
    }
    #[inline(always)]
    fn ne(&self, other: &fff) -> bool {
        self.ne(&other.0)
    }
}

impl Ord for fff {
    #[inline(always)]
    fn cmp(&self, other: &fff) -> Ordering {
        self.0.partial_cmp(&other.0).expect("fff")
    }
}

impl PartialOrd<f32> for fff {
    #[inline(always)]
    fn partial_cmp(&self, other: &f32) -> Option<Ordering> {
        self.0.partial_cmp(other)
    }
}

impl PartialOrd<fff> for f32 {
    #[inline(always)]
    fn partial_cmp(&self, other: &fff) -> Option<Ordering> {
        self.partial_cmp(&other.0)
    }
}

impl From<f32> for fff {
    #[inline(always)]
    fn from(v: f32) -> Self {
        debug_assert!(v.is_finite());
        fff(v)
    }
}

impl From<fff> for f32 {
    #[inline(always)]
    fn from(v: fff) -> Self {
        v.0
    }
}

impl Deref for fff {
    type Target = f32;

    #[inline(always)]
    fn deref(&self) -> &f32 {
        &self.0
    }
}

impl fmt::Display for fff {
    #[inline(always)]
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(fmt)
    }
}

impl fmt::Debug for fff {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(fmt)?;
        fmt.write_char('f')
    }
}

#[test]
fn add_mul() {
    let a = fff(2.);
    let b = fff(2.5);
    let c: fff = 4.5f32.into();
    let d: fff = 5f32.into();
    assert_eq!(c, a+b);
    assert_eq!(d, a*b);
    assert_eq!(d, b+b);
}

#[test]
fn div_sub() {
    let a = fff(-9.);
    let b = fff(999.);
    let c: fff = (-111f32).into();
    let d: fff = 1008f32.into();
    assert_eq!(c, b/a);
    assert_eq!(d, b-a);
    assert_eq!(9.fff(), -a);
}

#[test]
fn deref() {
    let a = fff(1.);
    assert_eq!(false, a.is_sign_negative());
}

#[test]
fn into_eq() {
    let a: fff = 3f32.into();
    let b: f32 = a.into();
    let c = b.fff();
    assert_eq!(3., a);
    assert_eq!(c, b);
    assert_eq!(b, c);
}

#[test]
fn ord_max() {
    let a = 1f32.fff();
    assert!(a > 0f32);
    assert!(0f32 < a);
    assert!(a < 2f32);
    assert!(a > 0.9f32);
    assert!(a > 0.9f32.fff());

    assert_eq!(2., a.max(2.));
    assert_eq!(1., a.min(1.));
}

#[test]
fn round() {
    let a = 1.1f32.fff();
    assert_eq!(a.round(), 1.fff());
    assert_eq!(1.5.fff().round(), 2.fff());
    assert_eq!(1.5.fff().floor(), 1.fff());
    assert_eq!(1.4.fff().round(), 1.fff());
    assert_eq!(1.4.fff().ceil(), 2.fff());
    assert_eq!(-1.5.fff().round(), -2.fff());
    assert_eq!(-1.4.fff().round(), -1.fff());
    assert_eq!(-1.6.fff().round(), -2.fff());
}

#[test]
fn trunc() {
    let a = 1.1f32.fff();
    assert_eq!(a.trunc(), 1.fff());
    assert_eq!(1.5.fff().trunc(), 1.fff());
    assert_eq!(1.4.fff().trunc(), 1.fff());
    assert_eq!(-1.5.fff().trunc(), -1.fff());
    assert_eq!(-1.4.fff().trunc(), -1.fff());
    assert_eq!(-1.6.fff().trunc(), -1.fff());
}

#[test]
fn display_debug() {
    assert_eq!("1.5 -3.5f", format!("{} {:?}", fff(1.5), fff(-3.5)));
}
